import {Paciente} from '../shared/paciente.model';
import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http'; 
import { Observable, pipe } from 'rxjs';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';



@Injectable({
    providedIn:'root'
})
export class PacienteCadastroService{

    constructor(private http: HttpClient){}

    public cadastrarPaciente(paciente: Paciente): Observable<any>{
   
        return this.http.post<any>(`${environment.apiURL}/pacientes`,paciente).pipe(map((resposta: Response) => console.log(resposta)))
    };

}
