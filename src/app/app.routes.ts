import {Routes} from "@angular/router";
import { PacienteCadastroComponent } from './paciente-cadastro/paciente-cadastro.component';
import { HomeComponent } from './home/home.component';

export const ROUTES: Routes = [
    {path:"", component:HomeComponent},
    {path:"cadastrar-paciente", component: PacienteCadastroComponent}
]