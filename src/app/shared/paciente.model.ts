import { Endereco } from './endereco.model';
import { Contato } from './contato.model';

export class Paciente{
    constructor(){}
    public nome: string;
    public email: string; 
    public sexo: string = 'F' ;
    public cpf: number;
    public dataNascimento: Date;
    public enderecos: Endereco;
    public contatos: Contato;

    
}