import { Component, OnInit } from '@angular/core';
import {PacienteCadastroService} from './paciente-cadastro.service';
import {Paciente} from '../shared/paciente.model'
import { Contato } from '../shared/contato.model';
import { Endereco } from '../shared/endereco.model';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';





@Component({
  selector: 'app-paciente-cadastro',
  templateUrl: './paciente-cadastro.component.html',
  styleUrls: ['./paciente-cadastro.component.css'],
  providers:[PacienteCadastroService]
})
export class PacienteCadastroComponent implements OnInit {
  // O formGroup será conectado ao nosso formulario
  // Os formsControl que estara sendo passado como parametro dos FormsGroup são so elementos dos formularios
  
  formulario: FormGroup; 
 
  constructor(
    private cadastroPacienteService: PacienteCadastroService,
    private fb:FormBuilder
    ) {

    }

  ngOnInit() {
    this.inicializarFormulario();
  }

  public confirmarCadastro(): void{

    if(this.formulario.status === 'VALID'){
      let paciente: Paciente = new Paciente();
      let contato: Contato = new Contato();
      let end: Endereco = new Endereco();
      paciente.nome = this.formulario.value.nome
      paciente.sexo = this.formulario.value.sexo
 
      console.log(paciente)        
      this.cadastroPacienteService.cadastrarPaciente(paciente)
      .subscribe()
    }else{
      console.log("Está invalido")
     
      this.formulario.get('nome').markAsTouched();
    }
  }

    public inicializarFormulario(): void{
      this.formulario = this.fb.group({
         nome: [null,[ Validators.required]],  //validator serve para validar com os metodos existentes
         sexo: ['M',[Validators.required]]
        }) 
     }
 
  }

