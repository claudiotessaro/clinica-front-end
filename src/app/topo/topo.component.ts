import { Component, OnInit } from '@angular/core';

declare var $:any;

@Component({
  selector: 'app-topo',
  templateUrl: './topo.component.html',
  styleUrls: ['./topo.component.css']
})
export class TopoComponent implements OnInit {

  title = "Gerenciador Clinico";


  constructor() { }

  ngOnInit() {
  }

  showMenu(){
    $(document).ready(function () {
      $('#sidebarCollapse').click(function () {
          $('#sidebar').toggleClass('active');
      });

  });
}

}
